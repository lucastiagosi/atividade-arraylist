package test.JUnit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import junit.framework.Assert;
import p3.arraylist.Arraylist;
import p3.arraylist.ValorInvalidoException;

class ArraylistTest {

	Arraylist a;
	@BeforeEach
	void setUp() throws Exception {
		a = new Arraylist();
		a.inserirValor("maria");
		a.inserirValor("douglas");
		a.inserirValor("lucas");
		a.inserirValor("fernanda");
		a.inserirValor("marcos");
	}

	@AfterEach
	void tearDown() throws Exception {
		a = null;
	}

	@Test
	void isEmptyTest() {
		Assert.assertEquals(false, a.isEmpty());
	}
	@Test
	void tamanhoTest() {
		Assert.assertEquals(5, a.size());
	}
	@Test
	void valorPosicaoTest() {
		Assert.assertEquals("lucas", a.valorPosicao(2));
	}
	@Test
	void modificarValorTest() {
		Assert.assertEquals("tulio", a.modificarValor(4, "tulio"));
	}
	@Test
	void posicaoElementoTest() throws ValorInvalidoException {
		Assert.assertEquals(3, a.posicaoElemento("fernanda"));
	}
	/*@Test
	void removerElementoTest() {
		
	}
	@Test
	void removerElementoPosTest() {
		
	}
	*/

}
