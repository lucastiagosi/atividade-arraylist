package p3.arraylist;

public class Arraylist {
	private static final int TAMANHO = 3 ;
	private String[] elementos = new String[TAMANHO] ;
	private int pos;
	
	/**
	 * este m�todo insere valores para a lista 
	 * @author lucastiago
	 * @param valor
	 */
	public void inserirValor(String valor) 
	{
		if(pos >= elementos.length) 
		{
			String[] arraytemp = new String[elementos.length * TAMANHO];
			for (int i = 0; i <	elementos.length; i++) {
				arraytemp[i] = elementos[i] ;
			}
			elementos = arraytemp;
		}
		elementos[pos] = valor;
		pos++;
	}
	
	/*
	 * verifica se a lista � vazia
	 */
	public boolean isEmpty() 
	{
		boolean vazio = false;
		for (int i = 0; i < elementos.length; i++) {
			if(elementos[i] == "") 
			{
				vazio = true;
			}
		}
		return vazio;
	}
	
	/**
	* tamanho da lista
	*/
	public int size() 
	{
		return pos;
	}
	/**
	 * retorna o valor da posi��o desejada
	 * @param valor
	 * @return
	 */
	public String valorPosicao(int valor) 
	{
		
		return elementos[valor];
	}
	/**
	 * modifica o valor de uma dada posi��o
	 * @param valor
	 * @param novo
	 * @return
	 */
	public String modificarValor(int valor, String nome) 
	{
		return elementos[valor] = nome;
	}
	
	/**
	 * obter a posi��o de um dado elemento
	 */
	public int posicaoElemento(String valor) throws ValorInvalidoException
	{
		int posicao = 0;
		boolean verificador = false;
		for (int i = 0; i < elementos.length; i++) {
			
			if(elementos[i] == valor) 
			{
				posicao = i;
				verificador = true;
			}
		}
		
		if(verificador == false) 
		{
			throw new ValorInvalidoException();
		}
		
		return posicao ;
	}
	/**
	 * remove elementos a partir do valor
	 * @param valor
	 */
	public void removerElemento(String valor)
	{
		for (int i = 0; i < elementos.length; i++) {
			if(valor == elementos[i]) 
			{
				elementos[i] = null;
			}
		}
	}
	/**
	 * remove elementos a partir da posicao
	 * @param posicao
	 */
	public void removerElementoPos(int posicao) throws PosicaoInvalidaException
	{
		if(posicao > pos) 
		{
			throw new PosicaoInvalidaException();
		}
		elementos[posicao] = null;
		
	}

}