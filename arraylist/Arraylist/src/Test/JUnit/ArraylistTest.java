package Test.JUnit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import Arraylist.Arraylist;
import Arraylist.ValorInvalidoException;

class ArraylistTest {
	Arraylist al;
	@BeforeEach
	void setUp() throws Exception {
		al = new Arraylist();
		al.addValor(15);
		al.addValor(23);
		al.addValor(25.5);
		al.addValor(36);
	}

	@AfterEach
	void tearDown() throws Exception {
		al = null;
	}

	@Test
	void removerTest() throws ValorInvalidoException {
		al.removerValor(36);
		assert.assertEquals(0, al.getValorPosicao());
	}

}
